#!/usr/bin/python
# encoding: utf-8

import cv2
import numpy as np
import os
import sys


def crop(img, x1, y1, x2, y2):
    # note this is a ref
    return img[y1:y2, x1:x2, :]


def filter_(path, dict):
    file = []

    origin_path = os.path.abspath(os.curdir)
    os.chdir(path)
    path = os.path.abspath(os.curdir)
    for dir in os.listdir(path):
        subpath = os.path.join(path, dir)
        if os.path.isdir(subpath):
            for item in filter_(subpath, dict):
                file.append(item)
        else:
            head, tail = os.path.splitext(dir)
            if tail in dict:
                file.append(subpath)

    os.chdir(origin_path)
    return file


def parse_annotation(file):
    """Parse annotation for one image

    p_i => (x_i, y_i)
    tile => [p_0, p_1, p_2, p_3] (clockwise)

    Args:
        file: File object for annotation file

    Returns:
        tiles: [tile_0, tile_1, ..., tile_n]
    """
    def get_point(line):
        x, y = line.split(" ")
        return int(x), int(y)

    def get_bool(line):
        assert line in {"True", "False"}

    def skip():
        while True:
            line = file.next().strip()
            if line:
                break
        return line

    tiles = []
    try:
        while True:
            tile = []
            for i in xrange(4):
                line = skip()
                tile.append(get_point(line))
            line = skip()
            get_bool(line)
            tiles.append(tile)
    except StopIteration:
        pass
    return tiles


def get_bounding_box(tile):
    """Get bounding box giving a tile

    Args:
        tile: [p_0, p_1, p_2, p_3](clockwise)

    Returns:
        bb: a bounding box surrounding the tile

    """
    def aux(fun, index):
        def _aux(x, y):
            if fun(x[index], y[index]) is x[index]:
                return x
            else:
                return y
        return _aux
    x_min = reduce(aux(min, 0), tile)[0]
    x_max = reduce(aux(max, 0), tile)[0]
    y_min = reduce(aux(min, 1), tile)[1]
    y_max = reduce(aux(max, 1), tile)[1]
    return x_min, y_min, x_max, y_max


def parse_crop_annotation(image, file):
    """Parse and crop one annotation.

    Args:
        image: Image to be cropped
        file: File object for annotation file

    Returns:
        cropped_images: Cropped image patches
    """
    tiles = parse_annotation(file)
    cropped_images = [crop(image, *get_bounding_box(tile)) for tile in tiles]
    return cropped_images


def add_uighur():
    """Example for adding Uighur to SIW-13 dataset."""
    cl = "Uighur"
    origin_abs_path = os.path.join(
        os.path.expanduser("~"), "documents", "datasets", cl)
    dataset_path = os.path.join(os.path.expanduser(
        "~"), "documents", "datasets", "siw")
    train_file_path = os.path.join(dataset_path, "train-list.txt")
    test_file_path = os.path.join(dataset_path, "test-list.txt")
    image_paths = filter_(origin_abs_path, [".jpg"])
    anno_paths = [path + ".txt" for path in image_paths]

    test_up_bound = 500
    test_count = 0

    cropped_images = []
    for image, anno in zip(image_paths, anno_paths):
        try:
            with open(anno, "r") as file:
                image = cv2.imread(image, cv2.IMREAD_COLOR)
                cropped_images.append(parse_crop_annotation(image, file))
        except IOError:
            print(anno)
    with open(train_file_path, "a") as train_file:
        with open(test_file_path, "a") as test_file:
            for i, images in enumerate(cropped_images):
                for j, image in enumerate(images):
                    path = os.path.join(
                        cl, "{}_{:0>6}_{}.jpg".format(cl, i + 1, j + 1))
                    cv2.imwrite(os.path.join(dataset_path, path), image)
                    if test_up_bound > test_count and np.random.uniform() > 0.5:
                        test_count += 1
                        test_file.write("\n" + path + " 14")
                    else:
                        train_file.write("\n" + path + " 14")


def assure_dir(dir):
    if not os.path.exists(dir):
        os.mkdir(dir)


def output_to_dir(image, anno, dir):
    """Output the image tiles into the dir.

    dir
        path.basename
            patch_0
            patch_1
            ...
            patch_n

    Args:
        image: Path for image to be cropped.
        anno: Annotation for this image.
        dir: Output directory.

    Returns:
        None

    """
    with open(anno, "r") as file:
        img = cv2.imread(image, cv2.IMREAD_COLOR)
        cropped_images = parse_crop_annotation(img, file)
    base_name = os.path.basename(image)
    subdir, _ = os.path.splitext(base_name)
    abs_subdir = os.path.join(dir, subdir)
    assure_dir(abs_subdir)
    for i, image in enumerate(cropped_images):
        path = os.path.join(abs_subdir, "{}_{}.png".format(subdir, i))
        cv2.imwrite(path, image)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python input_dir")
        exit(1)
    input_dir = sys.argv[1]
    image_paths = filter_(input_dir, [".jpg", ".png"])
    anno_paths = [path + ".txt" for path in image_paths]
    output_root_dir = "/tmp/script_identification_{}".format(os.getpid())
    print(output_root_dir)
    assure_dir(output_root_dir)
    for image, anno in zip(image_paths, anno_paths):
        output_to_dir(image, anno, output_root_dir)
