import os
import lmdb  # install lmdb by "pip install lmdb"
import cv2
import numpy as np


def checkImageIsValid(imageBin):
    if imageBin is None:
        return False
    imageBuf = np.fromstring(imageBin, dtype=np.uint8)
    img = cv2.imdecode(imageBuf, cv2.IMREAD_GRAYSCALE)
    imgH, imgW = img.shape[0], img.shape[1]
    if imgH * imgW == 0:
        return False
    return True


def getRatio(imageBin):
    if imageBin is None:
        return False
    imageBuf = np.fromstring(imageBin, dtype=np.uint8)
    img = cv2.imdecode(imageBuf, cv2.IMREAD_GRAYSCALE)
    imgH, imgW = img.shape[0], img.shape[1]
    return imgH / float(imgW)


def writeCache(env, cache):
    with env.begin(write=True) as txn:
        for k, v in cache.iteritems():
            #  print(type(k), type(v))
            txn.put(k, v)


def getFileList(path):
    file = open(path)
    images = []
    labels = []
    for line in file.readlines():
        img, label = line.split(' ')
        images.append(img)
        labels.append(label)
    return images, labels


def createDataset(outputPath, imagePathList, labelList, checkValid=True):
    """
    Create LMDB dataset for CRNN training.

    ARGS:
        outputPath    : LMDB output path
        imagePathList : list of image path
        labelList     : list of corresponding groundtruth texts
        lexiconList   : (optional) list of lexicon lists
        checkValid    : if true, check the validity of every image
    """
    assert(len(imagePathList) == len(labelList))
    nSamples = len(imagePathList)
    env = lmdb.open(outputPath, map_size=1099511627776)
    cache = {}
    data = []
    cnt = 1
    for i in xrange(nSamples):
        imagePath = imagePathList[i]
        label = labelList[i]
        if not os.path.exists(imagePath):
            print('%s does not exist' % imagePath)
            continue
        with open(imagePath, 'r') as f:
            imageBin = f.read()
        if checkValid:
            if not checkImageIsValid(imageBin):
                print('%s is not a valid image' % imagePath)
                continue
        data.append((imageBin, label))
    ratios = [getRatio(item[0]) for item in data]
    items = list(zip(ratios, data))
    items.sort(key=lambda item: item[0])

    for i in xrange(len(items)):
        imageKey = 'image-%09d' % cnt
        labelKey = 'label-%09d' % cnt
        cache[imageKey] = items[i][1][0]
        cache[labelKey] = items[i][1][1]
        if cnt % 1000 == 0 or cnt == nSamples:
            writeCache(env, cache)
            cache = {}
            print('Written %d / %d' % (cnt, nSamples))
        cnt += 1
    nSamples = cnt - 1
    cache['num-samples'] = str(nSamples)
    writeCache(env, cache)
    print('Created dataset with %d samples' % nSamples)


def create(path, outputPath):
    train_path = path + 'train-list.txt'
    test_path = path + 'test-list.txt'
    images, labels = getFileList(train_path)
    images = [os.path.join(path, file) for file in images]
    createDataset(outputPath + 'train', images, labels, checkValid=True)
    images, labels = getFileList(test_path)
    images = [os.path.join(path, file) for file in images]
    createDataset(outputPath + 'test', images, labels, checkValid=True)


if __name__ == '__main__':
    dataset_path = os.path.join(os.path.expanduser("~"), 'documents/datasets/siw/')
    create(dataset_path, '../data/')
