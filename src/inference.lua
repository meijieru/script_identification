require('cutorch')
require('lfs')
require('io')
require('nn')
require('cunn')
require('cudnn')
require('optim')
require('paths')
require('nngraph')

require('utilities')
require('DatasetLmdb')
require('LstmLayer')
require('BiRnnJoin')
require('WeightCombineTable')


cutorch.setDevice(1)
torch.setnumthreads(4)
torch.setdefaulttensortype('torch.FloatTensor')

local modelDir = '../model/script_identification/'
paths.dofile(paths.concat(modelDir, 'config.lua'))
local modelLoadPath = paths.concat(modelDir, 'snapshot_40000.t7')
gConfig = getConfig()
gConfig.modelDir = modelDir
gConfig.maxT = 0
local model, criterion = createModel(gConfig)
local snapshot = torch.load(modelLoadPath)
local label2str = label_to_str_map('siw')
loadModelState(model, snapshot)
model:evaluate()

local imageRoot = arg[1]

for dir in lfs.dir(imageRoot) do
    if dir ~= '.' and dir ~= ".." then
        local d = imageRoot..'/'..dir
        local preds = torch.Tensor(gConfig.nClasses):fill(0)
        for image in lfs.dir(d) do
            if image ~= '.' and image ~= ".." then
                local imagePath = d..'/'..image
                print(imagePath)
                local img = loadAndResizeImage(imagePath)
                -- local text, raw = recognizeImageLexiconFree(model, img)
                local img = img:reshape(1, img:size(1), img:size(2), img:size(3))
                local output = model:forward(img)
                local max_prob, max_index = torch.max(output, 2)
                max_index = max_index:reshape(max_index:size(1))[1]
                preds[max_index] = preds[max_index] + 1
            end
        end
        local index_count, final_index = torch.max(preds, 1)
        index_count, final_index = index_count[1], final_index[1]
        local certainty = index_count / preds:sum()
        file = io.open(d..'/'..'result.txt', 'w')
        file:write(string.format("Voting to %s with certainty %f", label2str[final_index], certainty))
    end
end
