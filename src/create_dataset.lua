require 'torch'
require 'nn'
require 'image'
require 'lfs'
require 'math'
require 'os'

-- Note: the data, in X, is 4-d: the 1st dim indexes the samples, the 2nd
-- dim indexes the color channels (RGB), and the last two dims index the
-- height and width of the samples.

function preprocess(img, height, width)
    -- if img:size(1) == 3 then
        -- img = image.rgb2y(img)
    -- end
    img = image.scale(img, width, height)
    return img
end


-- TODO(jrmei) : should keep the height / width rate ?
function create_dataset(path, save_dir, shape_image)
    -- this function return the dataset
    print("Creating Dataset")

    local label = 0
    local index = 0
    local label_translate = {}
    local label_map = {}

    local train_image_table = {}
    local train_label_table = {}
    local test_image_table = {}
    local test_label_table = {}

    local height = 0
    local width = 0

    for dir in lfs.dir(path) do
        if dir ~= "." and dir ~= ".." and dir ~= 'test-list.txt' and dir ~= 'train-list.txt' then
            label = 1 + label
            local d = path..'/'..dir
            label_translate[label] = dir
        end
    end
    for i = 1, #label_translate do
        label_map[label_translate[i]] = i
    end

    local test_file = io.open(path .. 'test-list.txt')
    local train_file = io.open(path .. 'train-list.txt')

    function readImg(file, image_table, label_table)
        for line in file:lines() do
            img_name = string.split(line, " ")[1]
            label = label_map[string.split(img_name, "/")[1]]
            img_name = path .. img_name
            img = image.loadJPG(img_name, 3, 'byte')

            index = index + 1
            height = height + img:size(2)
            width = width+ img:size(3)
            img = preprocess(img, shape_image[1], shape_image[2])
            image_table[#image_table + 1] = img
            label_table[#label_table + 1] = label

            if index ~= 0 and index % 1000 == 0 then
                print(index)
                collectgarbage('collect')
            end
        end
    end

    readImg(train_file, train_image_table, train_label_table)
    readImg(test_file, test_image_table, test_label_table)

    function saveData(save_path, image_table, label_table)
        local data_tensor = torch.ByteTensor(#image_table, image_table[1]:size(1), shape_image[1], shape_image[2]):fill(0)
        local label_tensor = torch.ByteTensor(#image_table)
        for i = 1, #image_table do
            data_tensor[{ {i} }] = image_table[i]
            label_tensor[{ i }] = label_table[i]
        end
        local data = {images = data_tensor, labels = label_tensor, label_translate = label_translate}
        torch.save(save_path .. '/' .. 'data.t7', data)
    end

    saveData(save_dir .. 'train', train_image_table, train_label_table)
    saveData(save_dir .. 'test', test_image_table, test_label_table)
end


print("data load and pre-process")
create_dataset("/home/jrmei/dataset/siw/",
               "/home/jrmei/research/crnn/data/", {32, 128})
