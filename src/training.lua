function trainModel(model, criterion, trainSet, testSet)
    -- get model parameters
    local params, gradParams = model:getParameters()
    local optimMethod = gConfig.optimMethod
    local optimState = gConfig.optimConfig

    function trainBatch(inputBatch, targetBatch, confusion)
        --[[ One step of SGD training
        ARGS:
          - `inputBatch`  : batch of inputs (images)
          - `targetBatch` : batch of targets (groundtruth labels)
        ]]
        model:training()
        local nFrame = #inputBatch
        local feval = function(p)
            if p ~= params then
                params:copy(x)
            end
            gradParams:zero()
            local gradParamsCopy = gradParams:clone()
            local f = 0

            for idx = 1, #inputBatch do
                local input = inputBatch[idx]:reshape(1, inputBatch[idx]:size(1), inputBatch[idx]:size(2),
                                                      inputBatch[idx]:size(3))
                local output = model:forward(input)
                model:backward(input, criterion:backward(output, targetBatch[idx]))
                f = f + criterion:forward(output, targetBatch[idx])
                gradParamsCopy = gradParamsCopy + gradParams
                gradParams:zero()

                -- update the confusion matrix
                local _, output_index = torch.max(output, 2)
                output_index = output_index[1][1]
                confusion:add(output_index, targetBatch[idx])
            end

            gradParams = gradParamsCopy:div(nFrame)
            f = f / nFrame
            return f, gradParams
        end
        local _, loss = optimMethod(feval, params, optimState); loss = loss[1]
        return loss
    end

    function validation(input, target)
        --[[ Do validation
        ARGS:
          - `input`  : validation inputs
          - `target` : validation targets
        ]]
        logging('Validating...')
        model:evaluate()

        -- batch feed forward
        -- local batchSize = gConfig.valBatchSize
        local nFrame = #input
        local output = torch.Tensor(nFrame, gConfig.nClasses)
        for i = 1, #input do
            print(i)
            local img = input[i]:reshape(1, input[i]:size(1), input[i]:size(2), input[i]:size(3))
            local output_img = model:forward(img)
            output[i] = output_img
        end

        -- compute loss
        max_prob, max_index = torch.max(output, 2)
        max_index = max_index:reshape(max_index:size(1))

        local confusion = optim.ConfusionMatrix(gConfig.nClasses)
        for i = 1, nFrame do
            confusion:add(max_index[i], target[i])
        end
        logging(confusion)
        -- logging('ConfusionMatrix: ' .. confusion)
    end

    -- train loop
    local iterations = 0
    local loss = 0
    local confusion = optim.ConfusionMatrix(gConfig.nClasses)
    while true do
        -- validation
        if iterations == 0 or iterations % gConfig.testInterval == 0 then
            local valInput, valTarget = testSet:allImageLabel()
            validation(valInput, valTarget)
            collectgarbage()
            os.exit(0)
        end

        -- train batch
        local input, target = trainSet:nextBatch()
        assert(#input == gConfig.trainBatchSize)
        oneBatchLoss = trainBatch(input, target, confusion)
        loss = loss + oneBatchLoss
        iterations = iterations + 1

        -- display
        if iterations % gConfig.displayInterval == 0 then
            loss = loss / gConfig.displayInterval
            logging(string.format('Iteration %d - train loss = %f', iterations, loss))
            logging(confusion)
            diagnoseGradients(model:parameters())
            loss = 0
            confusion:zero()
            collectgarbage()
        end

        -- save snapshot
        if iterations > 0 and iterations % gConfig.snapshotInterval == 0 then
            local savePath = paths.concat(gConfig.modelDir, string.format('snapshot_%d.t7', iterations))
            torch.save(savePath, modelState(model))
            logging(string.format('Snapshot saved to %s', savePath))
            collectgarbage()
        end

        -- terminate
        if iterations > gConfig.maxIterations then
            logging('Maximum iterations reached, terminating ...')
            break
        end
    end
end
