local pathCache = package.path
package.path = '../third_party/lmdb-lua-ffi/src/?.lua'
local lmdb = require('lmdb')
package.path = pathCache
local Image = require('image')
require('randomkit')

local DatasetLmdb = torch.class('DatasetLmdb')


function DatasetLmdb:__init(lmdbPath, batchSize, imageType)
    self.batchSize = batchSize or -1
    self.imageType = imageType or 'jpg'
    self:loadDataset(lmdbPath)
end


function DatasetLmdb:loadDataset(lmdbPath)
    self.env = lmdb.environment(lmdbPath, {subdir=false, max_dbs=8, size=1099511627776})
    self.env:transaction(function(txn)
        self.nSamples = tonumber(tostring(txn:get('num-samples')))
    end)
end


function DatasetLmdb:getNumSamples()
    return self.nSamples
end


local function imageCrop(inputImage, cropWidth)
    local temp = randomkit.randint(0, inputImage:size(3)-cropWidth)
    local crop = image.crop(inputImage, temp, 0, temp + cropWidth, inputImage:size(2))
    local background = torch.ByteTensor(inputImage:size()):fill(128)
    background[{ {}, {}, {temp + 1, temp + cropWidth} }] = crop
    return background
end


local function dataAug(ratio, batchImages, batchLabels)
    --ratio: ratio * original width = crop width
    local numberImages = #batchImages
    local imageDistorted, labelDistorted = {}, {}
    for i = 1, numberImages do
        local img = batchImages[i]
        if math.random() < 0.5 then
            img = imageCrop(img, math.floor(ratio*batchImages[i]:size(3)))
        end
        if math.random() < 0.5 then
            img = image.hflip(img)
        end
        table.insert(imageDistorted, img)
        table.insert(labelDistorted, batchLabels[i])
    end
    return imageDistorted, labelDistorted
end


local function swap(array, index1, index2)
    array[index1], array[index2] = array[index2], array[index1]
end


local function shuffle(array1, array2)
    local counter = #array
    while counter > 1 do
        local index = math.random(counter)
        swap(array1, index, counter)
        swap(array2, index, counter)
        counter = counter - 1
    end
end


local function preprocessImage(img, height)
    img = Image.rgb2y(img)
    local scale = height / img:size(2)
    img = Image.scale(img, img:size(3) * scale, height)
    return img
end


local function getImageLabelIdx(txn, idx, imgH, distort)
    local imageKey = string.format('image-%09d', idx)
    local labelKey = string.format('label-%09d', idx)
    local imageBin = tostring(txn:get(imageKey))
    local labelBin = tostring(txn:get(labelKey))

    local imageByteLen = string.len(imageBin)
    local imageBytes = torch.ByteTensor(imageByteLen):fill(0)
    imageBytes:storage():string(imageBin)
    local img = Image.decompress(imageBytes, 3, 'byte')
    img = preprocessImage(img, imgH)
    return img, labelBin
end


function DatasetLmdb:allImageLabel(nSampleMax)
    local imgH = 32
    nSampleMax = nSampleMax or math.huge
    local nSample = math.min(self.nSamples, nSampleMax)
    if self.imageList == nil then
        self.imageList = {}
        self.labelList = {}
        self.env:transaction(function(txn)
            for i = 1, nSample do
                local img, labelBin = getImageLabelIdx(txn, i, imgH, false)
                self.imageList[i] = img
                self.labelList[i] = tonumber(labelBin)
            end
        end)
    end
    return self.imageList, self.labelList
end


function DatasetLmdb:nextBatch()
    local imgH = 32
    local randomStart = randomkit.randint(1, self.nSamples)
    local randomIndex = torch.LongTensor(self.batchSize):range(randomStart, self.batchSize + randomStart)
    local imageList, labelList = {}, {}

    -- load image binaries and labels
    local widths = torch.Tensor(self.batchSize)
    local success, msg, rc = self.env:transaction(function(txn)
        for i = 1, self.batchSize do
            local idx = randomIndex[i]
            local img, labelBin = getImageLabelIdx(txn, idx, imgH, true)
            imageList[i] = img
            labelList[i] = tonumber(labelBin)
            widths[i] = img:size(3)
        end
    end)
    local imageDistorted, labelDistorted = dataAug(0.7, imageList, labelList)
    collectgarbage('collect')
    return imageDistorted, labelDistorted
end
