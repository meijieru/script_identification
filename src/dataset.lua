require 'image'
require 'randomkit'
require 'math'

local Dataset = torch.class('Dataset')

function Dataset:__init(batchSize)
    self.batchSize = batchSize
    self.beginIndex = 1
    self.cycle = 0
    self.batchImagesCuda = nil
end

function Dataset:loadDataset(path)
    local dataset = torch.load(path)
    self.images = dataset.images
    self.labels = dataset.labels
    self.label_translate = dataset.label_translate
    self.nSamples = self.images:size(1)
    self.images = self.images:view(self.nSamples, self.images:size(2), self.images:size(3), self.images:size(4))
    -- selfNormalize()
end

function Dataset:getData()
    return self.images, self.labels
end

function Dataset:normalize(mean,std)
    self.images:float()
    self.images[{ {}, {}, {} }]:add(-mean)
    self.images[{ {}, {}, {} }]:div(std)
end

function Dataset:selfNormalize()
    self.images:float()
    local mean = self.images[{ {},{},{} }]:mean()
    local std = self.images[{ {}, {}, {} }]:std()
    self.images[{ {}, {}, {} }]:add(-mean)
    self.images[{ {}, {}, {} }]:div(std)
    return mean,std
end

function Dataset:nextBatch()
    self.randomIndex = torch.LongTensor():randperm(self.images:size(1)):narrow(1, 1, self.batchSize)
end

function Dataset:getBatch()
    self:nextBatch()

    local batchImages = self.images:index(1, self.randomIndex)
    local batchLabels = self.labels:index(1, self.randomIndex)


    function dataAug(ratio)
        --ratio: ratio * original width = crop width
        numberImages = #batchImages
        for i = 1, numberImages do
            if math.random() < 0.5 then
                croppedImage = imageCrop(batchImages[i], math.floor(ratio*batchImages[i]:size(2)))
                table.insert(batchImages, croppedImage)
                table.insert(batchLabels, batchLabels[i])
            end
            if math.random() < 0.5 then
                image_vfilp = image.vflip(batchImages[i])
                table.insert(batchImages, image_vfilp)
                table.insert(batchLabels, batchLabels[i])
            end
            if math.random() < 0.5 then
                image_hflip = image.hflip(batchImages[i])
                table.insert(batchImages, image_hflip)
                table.insert(batchLabels, batchLabels[i])
            end
        end
    end

    function imageCrop(inputImage, cropWidth)
        temp = randomkit.randint(0, inputImage:size(2)-cropWidth)
        crop = image.crop(inputImage, 
                        temp, 
                        0,
                        temp + cropWidth,
                        inputImage:size(1))
        sum = 0
        inputImage:apply(function(x)
                            sum = sum + x
                            end)
        background = torch.DoubleTensor(inputImage:size()):fill(sum/inputImage:storage():size())
        temp = randomkit.randint(0, inputImage:size(2)-cropWidth)
        for j = temp + 1, temp + cropWidth do
            for k = 1, background:size(1) do
                background[k][j] = crop[k][j-temp]
            end
        end
        return background
    end

    function swap(array, index1, index2)
        array[index1], array[index2] = array[index2], array[index1]
    end

    function shuffle(array1, array2)
        local counter = #array
        while counter > 1 do
            local index = math.random(counter)
            swap(array1, index, counter)
            swap(array2, index, counter)
            counter = counter - 1
        end
    end

    dataAug(0.7)
    shuffle(batchImages, batchLabels)

    return batchImages, batchLabels
end
