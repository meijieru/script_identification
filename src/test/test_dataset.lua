require('../dataset')
require('image')

local trainSet = Dataset(10)
local valSet = Dataset()
trainSet:loadDataset( '../data/train/data.t7')
valSet:loadDataset( '../data/test/data.t7')

function checkDataset(dataset)
    one_batch_image, one_batch_label = dataset:getBatch()
    local decode_map = dataset.label_translate
    for i = 1, one_batch_image:size(1) do
        -- itorch.image(one_batch_image[i])
        image.save(i .. '.jpg', one_batch_image[i])
    end
    local label = {}
    for i = 1, one_batch_image:size(1) do
        label[i] = decode_map[one_batch_label[i]]
    end
    print(label)
end

function checkUniform(dataset)
    for i = 1, 10000 do
        _, _ = dataset:getBatch()
        if i % 100 then
            collectgarbage('collect')
        end
    end
    print(dataset.index)
end

-- checkUniform(trainSet)
checkDataset(trainSet)
