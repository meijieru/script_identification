local pathCache = package.path
package.path = '../../third_party/lmdb-lua-ffi/src/?.lua'
local lmdb = require('lmdb')
package.path = pathCache
require('../DatasetLmdb')
require('image')


function testBatch()
    local trainSet = DatasetLmdb('/home/jrmei/research/crnn/data/train/data.mdb', 20)
    print(trainSet:getNumSamples())
    local images, labels = trainSet:nextBatch()
    print(images)
    print(labels)
    for i = 1, #images do
        image.save(i .. '.jpg', images[i])
    end
end


function testAll()
    local valSet = DatasetLmdb('/home/jrmei/research/crnn/data/test/data.mdb')
    print(valSet:getNumSamples())
    local images, labels = valSet:allImageLabel()
    assert(#images == valSet:getNumSamples(), 'len(image) not equal to "num-samples"')
    for i = 1, 20 do
        image.save('test' .. i .. '.jpg', images[i])
    end
end

testBatch()
-- testAll()
