require('cutorch')
require('nn')
require('cunn')
require('cudnn')
require('optim')
require('paths')
require('nngraph')

require('utilities')
require('DatasetLmdb')
require('LstmLayer')
require('BiRnnJoin')
require('WeightCombineTable')


cutorch.setDevice(1)
torch.setnumthreads(4)
torch.setdefaulttensortype('torch.FloatTensor')

print('Loading model...')
local modelDir = '../model/script_identification/'
paths.dofile(paths.concat(modelDir, 'config.lua'))
local modelLoadPath = paths.concat(modelDir, 'snapshot_40000.t7')
gConfig = getConfig()
gConfig.modelDir = modelDir
gConfig.maxT = 0
local model, criterion = createModel(gConfig)
local snapshot = torch.load(modelLoadPath)
local label2str = label_to_str_map('siw')
loadModelState(model, snapshot)
model:evaluate()
print(string.format('Model loaded from %s', modelLoadPath))

local imagePath = '../data/demo.jpg'
local img = loadAndResizeImage(imagePath)
-- local text, raw = recognizeImageLexiconFree(model, img)
local img = img:reshape(1, img:size(1), img:size(2), img:size(3))
local output = model:forward(img)
local max_prob, max_index = torch.max(output, 2)
max_index = max_index:reshape(max_index:size(1))[1]
print(string.format('Classify to be %s', label2str[max_index]))
