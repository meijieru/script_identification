Convolutional Recurrent Neural Network
======================================

This software implements the Scene Text Script Identification with Convolutional Recurrent Neural Networks.

Train a new model
-----------------

Follow the following steps to train a new model on your own dataset.

  1. Create a new LMDB dataset. A python program is provided in ``tool/create_dataset.py``. Refer to the function ``createDataset`` for details.
  2. Create model directory under ``model/``. For example, ``model/foo_model``. Then create 
   configuraton file ``config.lua`` under the model directory. You can copy ``model/crnn_demo/config.lua`` and do modifications.
  3. Go to ``src/`` and execute ``th main_train.lua ../models/foo_model/``. Model snapshots and logging file will be saved into the model directory.


Work with object detection architecture
---------------------------------------
Follow the following steps to classify the images:
    1. execute ``python tool/crop_gen.py src_path`` for generating correcponding image patches which will get back the absolute dir for the image patches
    2. execute ``th src/inference.lua src_path`` with src_path to be previous step's output which will outputs result for each image.

Acknowledgements
----------------

The authors would like to thank the developers of Torch7, TH++, [lmdb-lua-ffi](https://github.com/calind/lmdb-lua-ffi), [char-rnn](https://github.com/karpathy/char-rnn) and [crnn](https://github.com/bgshih/crnn).

Please let me know if you encounter any issues.
