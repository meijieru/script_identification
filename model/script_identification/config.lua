function getConfig()
    local config = {
        nClasses         = 13,
        maxT             = 164,
        displayInterval  = 100,
        testInterval     = 1000,
        nTestDisplay     = 15,
        trainBatchSize   = 16,
        valBatchSize     = 256,
        snapshotInterval = 10000,
        maxIterations    = 40000,
        optimMethod      = optim.adadelta,
        optimConfig      = {},
        trainSetPath     = '../data/train/data.mdb',
        valSetPath       = '../data/test/data.mdb',
    }
    return config
end

local _weights = {
    502, 583, 798, 721, 518, 742, 715, 529, 1061, 692, 531, 1722, 677
}
local _sum = 0
for i = 1, #_weights do
    _sum = _sum + _weights[i]
end
local weights = torch.Tensor(#_weights)
for i = 1, #_weights do
    weights[i] = _sum / _weights[i]
end
weights:div(weights:sum())

function createModel(config)
    local nc = config.nClasses
    local nt = config.maxT

    local ks = {3, 3, 3, 3, 3, 3}
    local ps = {1, 1, 1, 1, 1, 1}
    local ss = {1, 1, 1, 1, 1, 1}
    local nm = {64, 128, 128, 256, 256, 512}
    local nh = {256}

    function convRelu(i, batchNormalization)
        batchNormalization = batchNormalization or false
        local nIn = nm[i-1] or 1
        local nOut = nm[i]
        local subModel = nn.Sequential()
        local conv = cudnn.SpatialConvolution(nIn, nOut, ks[i], ks[i], ss[i], ss[i], ps[i], ps[i])
        subModel:add(conv)
        if batchNormalization then
            subModel:add(nn.SpatialBatchNormalization(nOut))
        end
        subModel:add(cudnn.ReLU(true))
        return subModel
    end

    function bidirectionalLSTM(nIn, nHidden, nOut, maxT)
        local fwdLstm = nn.LstmLayer(nIn, nHidden, maxT, 0, false)
        local bwdLstm = nn.LstmLayer(nIn, nHidden, maxT, 0, true)
        local ct = nn.ConcatTable():add(fwdLstm):add(bwdLstm)
        local blstm = nn.Sequential():add(ct):add(nn.BiRnnJoin(nHidden, nOut, maxT))
        return blstm
    end

    -- model and criterion
    local model = nn.Sequential()
    model:add(nn.Copy('torch.FloatTensor', 'torch.CudaTensor', false, true))
    model:add(nn.AddConstant(-128.0))
    model:add(nn.MulConstant(1.0 / 128))
    model:add(convRelu(1))      -- 32x128(width varies)
    model:add(cudnn.SpatialMaxPooling(2, 2, 2, 2))      -- 16x64
    model:add(convRelu(2))
    model:add(cudnn.SpatialMaxPooling(2, 2, 2, 2)) -- 8x32
    model:add(convRelu(3, true))
    model:add(convRelu(4))
    model:add(cudnn.SpatialMaxPooling(1, 2, 1, 2))      -- 4x32
    model:add(convRelu(5))
    model:add(cudnn.SpatialMaxPooling(2, 2, 2, 2)) -- 2x16
    model:add(convRelu(6, true))
    model:add(cudnn.SpatialMaxPooling(1, 2, 1, 2))  --1x16
    model:add(nn.View(512, -1):setNumInputDims(3))       -- 512x16
    model:add(nn.Transpose({2, 3}))                      -- 16x512
    model:add(nn.SplitTable(2, 3))
    model:add(bidirectionalLSTM(512, 256, 256, nt))
    model:add(bidirectionalLSTM(256, 256, 256, nt))
    model:add(nn.WeightCombineTable(nt))
    model:add(nn.Linear(256, nc))
    model:add(nn.ReLU())
    model:add(nn.LogSoftMax())
    model:add(nn.Copy('torch.CudaTensor', 'torch.FloatTensor', false, true))
    model:cuda()
    local criterion = nn.ClassNLLCriterion(weights)
    -- local criterion = nn.ClassNLLCriterion()

    return model, criterion
end
